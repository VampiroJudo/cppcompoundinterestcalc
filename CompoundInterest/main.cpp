#include <iostream>
#include <cmath>
using namespace std;

void promptValues(double &balance, double &interest,int &time, int &compound, int choice) {
    if(choice == 1) {
        cout << "Enter a starting balance: ";
        cin >> balance;
        
        cout << "Enter an interest rate: ";
        cin >> interest;
        
        cout << "Enter a Time(in years): ";
        cin >> time;
        
        cout << "Enter a compound amount per year (# of qtrs): ";
        cin >> compound;
    }
    else if(choice == 2) {
        cout << "Enter a starting balance: ";
        cin >> balance;
        
        cout << "Enter an interest rate: ";
        cin >> interest;
        
        cout << "Enter a Time(years): ";
        cin >> time;
        
        cout << "Enter a compound amount per year (# of qtrs): ";
        cin >> compound;
    }
}

double calculateFinalAmount(double principleAmount, double interestRate,int time, int compound) {
    
    return (principleAmount * pow((1 + (interestRate/compound)), (time * compound)));
}

double calculatePrincipleAmount(double finalAmount, double interestRate, int time, int compound) {
    
    return (finalAmount / pow(1 + (interestRate / compound), compound * time));
}


int main() {

    int choice;
    do {
        cout << endl;
        cout << "Welcome. Choose an option below: \n";
        cout << "--------------------------\n";
        cout << "[1] - Find Compound Interest\n";
        cout << "[2] - Find Principal Amount\n";
        cout << "[0] - EXIT Program\n";
        cout << "--------------------------\n";
        cin >> choice;
        
        double balance;
        double interest;
        int time;
        int compound;
        
        if(choice == 1) {
            promptValues(balance, interest, time, compound, 1);
            double result = calculateFinalAmount(balance, interest, time, compound);
            cout << "\n" << "Results: \n";
            cout << "Final Compounded Value: $" << result << "\n";
        }
        else if(choice == 2){
            promptValues(balance, interest, time, compound, 2);
            double result = calculatePrincipleAmount(balance, interest, time, compound);
            cout << "\n" << "Results: \n";
            cout << "Principle Value: $" << result << "\n";
        }
        else if(choice != 0) {
            
            cout << "Invalid Choice. Choose Again.";
        }
    } while(choice != 0);
    return 0;
}
